# Google Drive Downloader

`gdrivedl` downloads google drive files using file id or url

## In action

I'm using a helper script you'll find it in the samples dir

[![asciicast](https://asciinema.org/a/310438.svg)](https://asciinema.org/a/310438)

## Installation

1. Open Terminal
1. Run this command `wget -q https://gitlab.com/AbdElraoufSabri/gdrivedl/raw/master/INSTALL -O - | bash`

## Update

1. Open Terminal
1. Run this command `gdrivedl-update`

## Uninstallation

1. Open Terminal
1. Run this command `gdrivedl-uninstall`

## Tested on

- [ ] Ubuntu 20.04
- [x] Ubuntu 19.10
- [ ] Ubuntu 18.04
- [ ] Ubuntu 16.04

## That's it

Please feel free to open new PR
